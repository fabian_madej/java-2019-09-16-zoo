package fabian.task.animals.models.mammals;

import fabian.task.animals.Animal;

public abstract class Mammal extends Animal {

    private String fur_color;

    public abstract void eat();

    //setter
    public void setFur_color(String fur_color){
        this.fur_color=fur_color;
    }
    //getter
    protected String getFur_color(){
        return fur_color;
    }
}
