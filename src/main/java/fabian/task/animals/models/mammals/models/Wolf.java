package fabian.task.animals.models.mammals.models;

import fabian.task.animals.Animal;
import fabian.task.animals.models.eat_something.Eat_meat;
import fabian.task.animals.models.mammals.Mammal;

public class Wolf extends Mammal implements Eat_meat {

    @Override
    public void eat_meat() {
        System.out.println("Wolf "+getName()+" eat meat");
    }
    public void howl(){
        System.out.println("Wolf "+getName()+" howl");
    }
    public String toString(){
        return ("Wolf "+getName()+" "+getAge()+" "+getFur_color());
    }
    public String toFile(){
        return ("\nWolf\n"+getName()+"\n"+getAge()+"\n"+getFur_color());
    }

    @Override
    public void eat() {
        eat_meat();
    }
}
