package fabian.task.animals.models.birds.models;

import fabian.task.animals.models.birds.Bird;
import fabian.task.animals.models.eat_something.Eat_plant;

public class Parrot extends Bird implements Eat_plant {
    @Override
    public void eat_plant() {
        System.out.println("Parrot "+getName()+" eat plants");
    }
    public void chirping(){
        System.out.println("Parrot "+getName()+" chirping");
    }
    public String toString(){
        return ("Parrot "+getName()+" "+getAge()+" "+getPlumage_color());
    }
    public String toFile(){
        return ("\nParrot\n"+getName()+"\n"+getAge()+"\n"+getPlumage_color());
    }

    @Override
    public void eat() {
        eat_plant();
    }
}
