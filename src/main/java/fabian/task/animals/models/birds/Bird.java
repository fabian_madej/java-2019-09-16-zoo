package fabian.task.animals.models.birds;

import fabian.task.animals.Animal;

public abstract class Bird extends Animal {
    private String plumage_color;

    public abstract void eat();

    //setter
    public void setPlumage_color(String plumage_color){
        this.plumage_color=plumage_color;
    }
    //getter
    protected String getPlumage_color(){
        return plumage_color;
    }
}
