package fabian.task.animals.models.lizards.models;

import fabian.task.animals.models.eat_something.Eat_plant;
import fabian.task.animals.models.lizards.Lizard;

public class Iguana extends Lizard implements Eat_plant {
    @Override
    public void eat_plant() {
        System.out.println("Iguana "+getName()+" eat plants");
    }
    public void hisses(){
        System.out.println("Iguana "+getName()+" hisses");
    }
    public String toString(){
        return ("Iguana "+getName()+" "+getAge()+" "+getScales_color());
    }
    public String toFile(){
        return ("\nIguana\n"+getName()+"\n"+getAge()+"\n"+getScales_color());
    }

    @Override
    public void eat() {
        eat_plant();
    }
}
