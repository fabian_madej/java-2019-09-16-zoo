package fabian.task.animals.models.lizards;

import fabian.task.animals.Animal;

public abstract class Lizard extends Animal {
    private String scales_color;

    public abstract void eat();

    //setter
    public void setScales_color(String scales_color){
        this.scales_color=scales_color;
    }
    //getter
    protected String getScales_color(){
        return scales_color;
    }
}
