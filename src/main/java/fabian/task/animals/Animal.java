package fabian.task.animals;

public abstract class Animal {

    //name and age of animal
    private String name;
    private int age;

    //abstract method responsible for eating
    public abstract void eat();

    //Setters
    public void setName(String name){
        this.name =name;
    }
    public void setAge(int age){
        this.age =age;
    }
    //Getters
    protected String getName(){
        return name;
    }
    protected int getAge(){
        return age;
    }
}
