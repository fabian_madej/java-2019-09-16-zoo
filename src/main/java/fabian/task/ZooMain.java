package fabian.task;

import fabian.task.animals.Animal;
import fabian.task.animals.models.birds.Bird;
import fabian.task.animals.models.birds.models.Parrot;
import fabian.task.animals.models.eat_something.Eat_meat;
import fabian.task.animals.models.eat_something.Eat_plant;
import fabian.task.animals.models.lizards.models.Iguana;
import fabian.task.animals.models.mammals.models.Wolf;

import java.io.*;
import java.util.Arrays;

public class ZooMain {
    public static void main(String[] args) {
        System.out.println("Welcome to the ZOO");
        String path = "/home/student/IdeaProjects/java-2019-09-16-zoo/src/main/resources/Animals.txt";
        String pathB = "/home/student/IdeaProjects/java-2019-09-16-zoo/src/main/resources/AnimalsB.txt";
        //reading file and save to array
        Animal[]animals = read_file(path);
        Animal[]animalsB = animals;
        //show animals from array
        show_animals(animals);
        //save animals to file
        write_file(path,animals);
        //save animals to file
        write_fileB(pathB,animalsB);
        //reading file and save to array
        animalsB = read_fileB(pathB);

        System.out.print("\n chirping all\n");
        animals_chirping(animals);
        System.out.print("\n hisses all\n");
        animals_hisses(animals);
        System.out.print("\n howl all\n");
        animals_howl(animals);

        System.out.print("\n Eat all\n");
        eat_all(animals);

        System.out.print("\nEat meat\n");
        eat_meat(animals);

        System.out.print("\nEat plant\n");
        eat_plant(animals);
    }

    private static Animal[] read_fileB(String pathB) {
    //    try {
    //        FileInputStream fi = new FileInputStream(new File(pathB));
    //        ObjectInputStream oi = new ObjectInputStream(fi);
    //
    //        // Read object
    //        Animal[] animals = (Animal[]) oi.readObject();
    //
    //        oi.close();
    //        fi.close();
    //
    //        return animals;
    //    } catch (IOException | ClassNotFoundException e) {
    //        e.printStackTrace();
    //        return new Animal[0];
    //    }
        return new Animal[0];
    }

    private static void write_fileB(String pathB, Animal[] animalsB) {
    //    try {
    //        FileOutputStream f = new FileOutputStream(new File(pathB));
    //        ObjectOutputStream o = new ObjectOutputStream(f);
    //
    //        for(Animal animal :animalsB) {
    //            // Write objects to file
    //            o.writeObject(animal);
    //        }
    //        o.close();
    //        f.close();
    //
    //    } catch (FileNotFoundException e) {
    //        System.out.println("File not found");
    //    } catch (IOException e) {
    //        System.out.println("Error initializing stream");
    //    }
    }

    private static void animals_howl(Animal[] animals) {
        for(Animal animal :animals){
            if(animal instanceof Wolf){
                ((Wolf) animal).howl();
            }
        }
    }

    private static void animals_hisses(Animal[] animals) {
        for(Animal animal :animals){
            if(animal instanceof Iguana){
                ((Iguana) animal).hisses();
            }
        }
    }

    private static void animals_chirping(Animal[] animals) {
        for(Animal animal :animals){
            if(animal instanceof Parrot){
                ((Parrot) animal).chirping();
            }
        }
    }

    private static void eat_plant(Animal[] animals) {
        for(Animal animal :animals){
            if(animal instanceof Eat_plant){
                ((Eat_plant) animal).eat_plant();
            }
        }
    }

    private static void eat_meat(Animal[] animals) {
        for(Animal animal :animals){
            if(animal instanceof Eat_meat){
                ((Eat_meat) animal).eat_meat();
            }
        }
    }

    private static void eat_all(Animal[] animals) {
        for(Animal animal :animals){
            animal.eat();
        }
    }

    //method show_animals is used to show all
    //animals and take array of animals
    private static void show_animals(Animal[] animals){
        for(Animal animal :animals){
            try {
                System.out.println(animal.toString());
            }catch (NullPointerException e){
                System.out.println(e.getMessage());
            }
        }
    }
    //method write_file take path to file and array with animals
    //and save animals to file
    private static void write_file(String path, Animal[] animals){
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(path))){
            writer.write(String.valueOf(animals.length));
            for (Animal animal : animals) {
                if (animal instanceof Wolf) {
                    writer.write(((Wolf) animal).toFile());
                }else if(animal instanceof Parrot){
                    writer.write(((Parrot) animal).toFile());
                }else if(animal instanceof Iguana){
                    writer.write(((Iguana) animal).toFile());
                }
            }
        }catch (IOException ex){
            System.out.println(ex.getMessage());
        }
    }
    //method read_file take path to file
    //and return array with animals
    private static Animal[] read_file(String path) {
        // size is number of animals in file
        // is used to make array
        int size;
        //array catchArray is used to return
        //when try throw exception
        Animal[] catchArray = new Animal[0];
        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            //take number of animals
            size = Integer.parseInt(reader.readLine());
            //animals array is used to return result
            Animal[] animals = new Animal[size];
            //i is index used in loop while
            int i=0;
            //reading animals and save to array animals
            while (i<size){
                switch (reader.readLine()) {
                    case "Wolf": {
                        Wolf newWolf = new Wolf();
                        newWolf.setName(reader.readLine());
                        newWolf.setAge(Integer.parseInt(reader.readLine()));
                        newWolf.setFur_color(reader.readLine());
                        animals[i] = newWolf;
                        i++;
                        break;
                    }
                    case "Iguana": {
                        Iguana newIguana = new Iguana();
                        newIguana.setName(reader.readLine());
                        newIguana.setAge(Integer.parseInt(reader.readLine()));
                        newIguana.setScales_color(reader.readLine());
                        animals[i] = newIguana;
                        i++;
                        break;
                    }
                    case "Parrot": {
                        Parrot newParrot = new Parrot();
                        newParrot.setName(reader.readLine());
                        newParrot.setAge(Integer.parseInt(reader.readLine()));
                        newParrot.setPlumage_color(reader.readLine());
                        animals[i] = newParrot;
                        i++;
                        break;
                    }
                }
            }
            //return result
            return animals;
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return catchArray;
        }
    }
}